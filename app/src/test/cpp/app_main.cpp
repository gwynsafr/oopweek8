#include <gtest/gtest.h>
// #include "app.cpp"


TEST(AppMainTest, Check1to1) {
    EXPECT_EQ(42, 42);
}

// TEST(AppMainTest, CheckFilter) {
//     EXPECT_EQ(only_parentheses_left(123), "()");
// }

// TEST(AppMainTest, CheckValid) {
//     EXPECT_EQ(isValid(123), true);
// }

// TEST(AppMainTest, CheckUnclosedNotValid) {
//     EXPECT_EQ(isValid("(123"), false);
// }

// TEST(AppMainTest, CheckDiffNotValid) {
//     EXPECT_EQ(isValid("(123}"), false);
// }

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}